## Basic config file
RELEASE="s9"
TODAY=`/bin/date +%Y%m%d`
TODAY_FORMAT=`/bin/date +%Y-%m-%d`
YESTERDAY=`/bin/date +%Y%m%d --date='yesterday'`
DELAYED=`/bin/date +%Y%m%d --date="$PRODDATE"`
TOO_OLD=`/bin/date +%Y%m%d --date="$OLDESTDATE"`

SOURCE="/data/centos-stream/9-stream"
DESTINATION="/data/$PATH_DESTINATION"
SIG_RPMS="/data/centos-stream/SIGs/9-stream"
KOJI_RPMS="/data/internal/repos"
SNAPS_DIR="${RELEASE}-snapshots"
SNAPS="$DESTINATION/$SNAPS_DIR"
DEST=$SNAPS/.tmp.$TODAY
TODELETE=$SNAPS/.todelete.$TODAY
FINALDEST=$SNAPS/$TODAY

UPSTREAM_REPOS="BaseOS AppStream HighAvailability RT ResilientStorage CRB NFV"
SIG_REPOS="extras cloud messaging"
KOJI_REPOS="cern9-stable openafs9-stable"
SUPPORTED_ARCHES="x86_64 aarch64"
ALL_REPOS="CERN openafs $UPSTREAM_REPOS $SIG_REPOS"

LN_TESTING="testing"
WEBSITE="https://linux.cern.ch"
MAILMX="cernmx.cern.ch"

SHORTLIST_COUNT=4
MAX_PACKAGES_LIST=25
DANGEROUS_RPMS="centos-stream-release centos-stream-repos centos-gpg-keys epel-release elrepo-release"
HIGHLIGHT_RPMS="centos-stream-release kernel kernel-plus kernel-rt dbus glibc \
  cern-get-keytab hepix qemu-kvm systemd libvirt firewalld python sssd kmod-openafs"

addDays() {
  DAYS=${2:-1} # default to 1 day
  /bin/date +%Y%m%d --date="$1+$DAYS days"
}

diffRepos() {
  NEW=$1
  OLD=$2

  for f in $(/usr/bin/diff --new-line-format="" --unchanged-line-format="" \
      <(find $NEW -type f -iname "*.rpm" -printf "%f\n" | sort)  \
      <(find $OLD -type f -iname "*.rpm" -printf "%f\n" | sort)); do
    # We compare by rpm filename, but we need the full path to get the rpm metadata
    path=$(find $NEW -iname "$f" -printf "%P\n")
    rpm -qp --queryformat "$f;%{name};%{version};%{release};%{arch}\n" $NEW/$path 2>/dev/null
  done
}

snapshotKojiRepo() {
  REPO=$1
  for ARCH in $SUPPORTED_ARCHES; do
    NAME="${REPO%%9-stable}"
    if [[ $NAME == "cern" ]]; then
      NAME="CERN"
    fi

    # Create the new path
    mkdir -pv $DEST/$NAME/$ARCH

    # Copy the RPMs
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/Packages $DEST/$NAME/$ARCH
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/repodata $DEST/$NAME/$ARCH

    # Look for differences
    diffRepos $DEST/$NAME/$ARCH/Packages $SNAPS/$YESTERDAY/$NAME/$ARCH/Packages > $DEST/$NAME/$ARCH/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/$ARCH/.diff" ] || rm -f "$DEST/$NAME/$ARCH/.diff"

    # If we already have a Source directory, it's because we copied it for a previous $ARCH, so we can continue
    [[ -d $DEST/$NAME/Source ]] && continue

    # Copy sources (as 'Source' to match upstream repos)
    mkdir -pv $DEST/$NAME/Source
    cp -Rl $KOJI_RPMS/$REPO/source/SRPMS/* $DEST/$NAME/Source
    diffRepos $DEST/$NAME/Source $SNAPS/$YESTERDAY/$NAME/Source > $DEST/$NAME/Source/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/Source/.diff" ] || rm -f "$DEST/$NAME/Source/.diff"
  done
}

# Try to delete faster than just a straight `rm -rf`
quickDelete() {
  [ -e $1 ] || return 0
  echo "Deleting $1"
  time for d in `find $1 -depth -type d 2>/dev/null`; do
    pushd $d >/dev/null
    perl -e 'for(<*>){unlink}'
    popd >/dev/null
    rm -rf $d
  done
  rm -rfv $1
}

linkTargetTo() {
    # $LINK -> $TARGET
    local TARGET=$1
    local LINK=$2

    # If $TARGET is a symlink, find out what it ultimately points to and link to that instead.
    # There's a limit to how many symlinks can point to each other. The limit, of course, is 42.
    # We shouldn't hit it, but it's been known to happen...
    [[ -h $TARGET ]] && TARGET=$(realpath $TARGET --relative-to=$(dirname $LINK))

    ln -vfsT $TARGET $LINK
}
