To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [CS9]: Dangerous packages detected: automation stopped

Dear admins,

********************************
***  Manual action required  ***
********************************

The following dangerous packages have been detected in today's snapshot:

$PACKAGES

A .freeze.${RELEASE}all file has been created and links will not be updated.

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
