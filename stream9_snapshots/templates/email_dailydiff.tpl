To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [DIFF]: CS9 - $SHORTLIST [...]

Dear Linux admins,

Here's the current state of the symlinks:

$STATE

Today's CentOS Stream 9 (CS9) snapshot ($TODAY) contains the following packages:

--PACKAGES--

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)