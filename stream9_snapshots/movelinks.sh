#!/bin/bash

source /root/common.sh

getLinkDate() {
  readlink "$1" | sed "s/$SNAPS_DIR\///"
}

isLinkNotFrozen() {
  ! ([ -f "$DESTINATION/.freeze.${RELEASE}all" ] || [ -f "$DESTINATION/.freeze.$1" ])
}

setLink() {
  TARGET=$1
  SOURCE=$2
  TARGET_DATE=`echo $TARGET | sed "s/$SNAPS_DIR\///"`
  SOURCE_DATE=`getLinkDate $SOURCE`

  if isLinkNotFrozen $SOURCE && [[ $TARGET_DATE -gt $SOURCE_DATE ]]; then
    echo -n " Link "
    linkTargetTo $TARGET $SOURCE

    # Aggregate all the diffs between $SOURCE_DATE and $TARGET_DATE
    DATE=`addDays $SOURCE_DATE`
    while [[ $DATE -le $TARGET_DATE ]]; do
      for REPO in $ALL_REPOS; do
        CLEANREPO="${REPO/\//-}"
        for ARCH in $SUPPORTED_ARCHES; do
          DIFF=`find "$SNAPS/$DATE/$REPO/$ARCH/" -name .diff`
          cat "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}" $DIFF 2>/dev/null | sort -u -o "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}"
        done

        # If there's no diff, just delete the file
        [ -s "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}" ] || rm -f "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}"
      done
      DATE=`addDays $DATE`
    done

    return 0
  fi
  return 1
}

getKernelFamily() {
  SNAPSHOT=$1
  LATESTKERNEL=`find $SNAPSHOT/BaseOS -name 'kernel-core-*' | sort | tail -n1`
  KERNVERSION=`rpm --queryformat "%{version}\n" -qp $LATESTKERNEL`
  KERNSHORTREL=`rpm --queryformat "%{release}\n" -qp $LATESTKERNEL | cut -d. -f1`

  echo "${KERNVERSION}-${KERNSHORTREL}"
}

cd $DESTINATION

if [ -f "$DESTINATION/.freeze.${RELEASE}all" ]; then
  # A .freeze.s9all file exists, so validation or triggers failed
  # Let's make sure today's snapshot exists, even if it's as a symlink to yesterday
  cd $SNAPS
  linkTargetTo $YESTERDAY $TODAY

  echo ".freeze.${RELEASE}all exists, validation or trigger error. Not updating links."
  exit
fi

# No .freeze.s9all file, so all went well
# Move $TODAY snapshot to a directory to be later deleted
mv -v $FINALDEST $TODELETE 2>/dev/null
# Promote .tmp.$TODAY to $TODAY
mv -v $DEST $FINALDEST
# Relink .s9-latest to today's snapshot
linkTargetTo $SNAPS_DIR/$TODAY ".${RELEASE}-latest"

# Don't update links on holidays
/root/checkholidays.sh || exit

echo "Checking symlinks"

if [[ -f "$DESTINATION/.forcemove.${RELEASE}" ]]; then
  # We're being forced to move the production symlinks, accelerating the
  # deployment to production. Let's do some sanity checks first.
  FORCED=`cat "$DESTINATION/.forcemove.${RELEASE}"`
  echo "Found .forcemove.${RELEASE}, should move production symlinks to $SNAPS_DIR/$FORCED"
  if [[ ! -d "$SNAPS_DIR/$FORCED" ]]; then
    echo "That path does not exist or is not a directory, won't do it!"
    exit 1
  fi
  echo "Ok, directory exists, production symlinks will point to $FORCED"
  # Note that symlinks are *never* moved backwards in time, no matter
  # what the .forcemove.s9 file says. setLink() enforces this.
  DELAYED=$FORCED
  rm "$DESTINATION/.forcemove.${RELEASE}"
fi

TMPDIR=`mktemp -d`

# s9-testing is always today, s9 is always delayed
setLink $SNAPS_DIR/$TODAY "$RELEASE-$LN_TESTING"
SHORTEST_TEST=$?
# Only move the prod symlink on Wednesdays or if we're being forced to
if [[ $(date +%u) -eq 3 || -n "$FORCED" ]]; then
  setLink $SNAPS_DIR/$DELAYED "$RELEASE"
  SHORTEST_PROD=$?
fi

# Stream has no point releases, so we fake it by defining one when a new major
# kernel version (ie. 4.18.0-240 to 4.18.0-259) is released. We don't want to
# have too many of them, so we limit it to one per quarter.

# First, check if there's been a major kernel version change since the last point release
TODAY_KERNELFAM=$(getKernelFamily $SNAPS_DIR/$TODAY)
LAST_POINT_RELEASE=$(find . -maxdepth 1 -type l -name "${RELEASE}.*" | sort | tail -n1)
LAST_POINT_KERNELFAM=$(getKernelFamily $(readlink $LAST_POINT_RELEASE))

if [[ "$TODAY_KERNELFAM" != "$LAST_POINT_KERNELFAM" ]]; then
  # So, today could be the beginning of a new point release, unless one already exists for this quarter
  YQ="$(/bin/date +%Y.%q)" # something like 2021.q4
  NEW_POINT_RELEASE="${RELEASE}.${YQ}"
  if [[ ! -e "${NEW_POINT_RELEASE}" ]]; then
    # It does not, so we have a new point release!
    setLink $SNAPS_DIR/$TODAY $NEW_POINT_RELEASE
    # Add the kernel to the date so it also goes into the description
    /root/aims.sh $SNAPS_DIR/$TODAY "$TODAY - KERNEL $TODAY_KERNELFAM" "point" "${YQ}"
    # Now trigger Config's Rundeck job to define the new point release in Foreman
    /root/rundeck.sh "${YQ}"
  fi
fi

if [[ $SHORTEST_TEST == 0 ]]; then
  /root/regen-repos.sh "c${RELEASE}-${LN_TESTING}-.*"
  /root/sendemail.sh $TMPDIR $LN_TESTING
  /root/aims.sh $SNAPS_DIR/$TODAY $TODAY $LN_TESTING
fi

if [[ $SHORTEST_PROD == 0 ]]; then
  /root/regen-repos.sh "c${RELEASE}-.*" ".*9-build"
  /root/sendemail.sh $TMPDIR
  /root/aims.sh $SNAPS_DIR/$DELAYED $DELAYED
fi

# Clean up .diff:* files
find "$TMPDIR" -name '.diff:*' -delete
